export interface Item {
  label: string
}

type List = Item[];

export default List;