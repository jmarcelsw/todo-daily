import { Link, routes } from '@redwoodjs/router'
import Date from 'src/components/Date'
import Section from 'src/components/Section/Section';
import tw, { css } from 'twin.macro'
import Mode from 'src/types/Mode'
import List, { Item } from 'src/types/List'


const HomePage = () => {
  const [lists, setLists] = React.useState<List[]>([[{ label: '200 Push upss' }],]);
  const [mode, setMode] = React.useState<Mode>(Mode.display);
  const containerStyle = css`
    max-width: 500px;
 `;

  const toggleMode = () => {
    if (mode === Mode.display) {
      setMode(Mode.edit)
    } else {
      setMode(Mode.display)
    }
  }

  const changeList = () => {

  }


  return (
    <>
      <div css={[tw`mx-auto p-4`, containerStyle]} >
        <Date />
        <div tw="w-full flex justify-end">
          <div tw="px-4 py-2 cursor-pointer" onClick={toggleMode}>{mode === Mode.display ? "Edit" : "Done"}</div>
        </div>

        {lists.map((list) => <Section mode={mode} list={list} />)}

      </div>
    </>
  )
}

export default HomePage
