import { useTheme } from '@emotion/react'
import React, { useState } from 'react'
import Mode from 'src/types/Mode'
import List, { Item } from 'src/types/List'
import tw from 'twin.macro'
import { list } from 'postcss'

interface ListItemProps {
  mode: Mode,
  item: Item
}

const ListItem = (props: ListItemProps) => {
  const [oldText, setOldText] = useState("")
  const [text, setText] = useState(props.item.label)
  const onChange = (event: any) => {
    if (event.target.value) {
      setOldText(text)
    }
    setText(event.target.value)
  };

  return <div tw="flex w-full mb-2">
    <div tw="mr-2">
      <input type="checkbox" disabled={props.mode === Mode.edit} tw="rounded text-blue-500 w-12 h-12" />
    </div>
    <div tw="text-lg flex-1 flex items-center">
      {props.mode === Mode.edit ? <input tw="flex-1 text-lg" placeholder={oldText} type="text" value={text as string} onChange={onChange} /> : text}
    </div>
  </div>

}

enum SectionType {
  daily,
  justToday
}

const Button = () => {
  const [sectionType, setSectionType] = React.useState(SectionType.justToday);
  const toggleType = () => {
    if (SectionType.justToday == sectionType) {
      setSectionType(SectionType.daily)
    } else {
      setSectionType(SectionType.justToday)
    }
  }

  return <div tw="px-2 py-2 cursor-pointer w-3/12 select-none text-center" onClick={toggleType}>{sectionType === SectionType.justToday ? "Just Today" : "Daily"}</div>
}
interface SectionProps {
  mode: Mode,
  list: List
}

const Section = (props: SectionProps) => {

  const [name, setName] = React.useState('Daily');
  const [items, setItems] = React.useState([]);

  const onChange = (event: any) => {
    if (event.target.value) {
      // setOldText(name)
    }
    setName(event.target.value)
  };

  return (
    <div tw="mb-4">
      {props.mode === Mode.display
        ? <h2 tw="text-lg mb-2"> {name} </h2>
        : <div tw="flex mb-2">
          <Button />
          <input tw="w-9/12" type="text" value={name} onChange={onChange} />
        </div>}
      <div tw="w-full">
        {props.list.map((item: Item) => <ListItem mode={props.mode} item={item} />)}
        {props.mode === Mode.edit && <ListItem item={{ label: "" } as Item} mode={props.mode} />}
      </div>
    </div>
  )
}

export default Section
