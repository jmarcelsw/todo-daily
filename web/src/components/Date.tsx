import { DateTime } from 'luxon'
import tw from 'twin.macro'

const Date = () => <div css={tw`text-3xl flex w-full justify-center`} >
  {DateTime.local().toFormat("MMMM dd, yyyy")}
</div>
export default Date
