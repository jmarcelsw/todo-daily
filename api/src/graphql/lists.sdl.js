export const schema = gql`
  type List {
    id: Int!
    name: String!
    createdOn: DateTime!
    updatedOn: DateTime!
  }

  type Query {
    lists: [List!]!
  }

  input CreateListInput {
    name: String!
    createdOn: DateTime!
    updatedOn: DateTime!
  }

  input UpdateListInput {
    name: String
    createdOn: DateTime
    updatedOn: DateTime
  }

  type Mutation {
    createList(input: CreateListInput!): List
  }
`
