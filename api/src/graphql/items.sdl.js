export const schema = gql`
  type Item {
    id: Int!
    label: String
    checked: Boolean!
    list: List!
    listId: Int!
    createdOn: DateTime!
    updatedOn: DateTime!
  }

  type Query {
    items: [Item!]!
  }

  input CreateItemInput {
    label: String
    checked: Boolean!
    listId: Int!
    createdOn: DateTime!
    updatedOn: DateTime!
  }

  input UpdateItemInput {
    label: String
    checked: Boolean
    listId: Int
    createdOn: DateTime
    updatedOn: DateTime
  }
`
