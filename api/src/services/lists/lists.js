import { db } from 'src/lib/db'

export const lists = () => {
    return db.list.findMany()
}

export const createList = ({ input }) => {
    console.log(input);
    return db.list.create({ data: input })
}