import { db } from 'src/lib/db'

export const items = () => {
  return db.item.findMany()
}

export const Item = {
  list: (_obj, { root }) => db.item.findOne({ where: { id: root.id } }).list(),
}
